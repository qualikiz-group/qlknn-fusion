These are QuaLiKiz neural networks based on a novel NN architecture.
Currently contains dummy files, as this work is unpublished and
under testing, and thus these files should not be used, copied or
distributed! However, the networks are available for testing inside
the JETTO framework.
For more information contact Karel van de Plassche on
k.l.vandeplassche@differ.nl or karelvandeplassche@gmail.com
